const cron = require("node-cron");
const cleanUpSchedules = require("./services/cleanUpSchedulesServices");

//Agendamneto da limpeza

cron.schedule("0 0 * * * ", async () => {
  try {
    await cleanUpSchedules();
    console.log("Limpeza automatica executada");
  } catch (error) {
    console.error("Erro ao executar limpeza", error);
  }
});
