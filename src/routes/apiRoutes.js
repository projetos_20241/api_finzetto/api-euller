const router = require("express").Router();
const userController = require("../controller/userController");
const classroomController = require("../controller/classroomController");
const scheduleController = require("../controller/scheduleController");

//User
router.post("/user/", userController.createUser);
router.post("/user/login", userController.postLogin);
router.get("/user/", userController.getAllUsers);
router.get("/user/:id", userController.getUserById);
router.put("/user/:id", userController.updateUser);
router.delete("/user/:id", userController.deleteUser);

//Quadras
router.post("/quadras/", classroomController.createClassroom);
router.get("/quadras/", classroomController.getAllClassrooms);
router.get("/quadras/:number", classroomController.getClassroomById);

//Reservas
router.post("/reservas/", scheduleController.createSchedule);
router.get("/reservas/", scheduleController.getAllSchedules);
router.get("/reservas/:id", scheduleController.getSchedulesByIdClassroom);
router.get("/reservas/ranges/:id",scheduleController.getSchedulesByIdClassroomRanges);
router.delete("/reservas/:id", scheduleController.deleteSchedule);

module.exports = router;
